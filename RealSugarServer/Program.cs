﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;
using Lidgren;
using System.Threading;

namespace RealSugarServer
{
    class Program
    {
        public static int messages = 0;
        public static Dictionary<string, User> players = new Dictionary<string, User>(); // names, User object
        public static  Dictionary<NetConnection, User> connections = new Dictionary<NetConnection, User>(); //Connection,user associated
        public static  List<card> FightCards = new List<card>();
        public static List<Thread> lobbies = new List<Thread>();
        static void Main(string[] args)
        {
            //configure the Server
            NetPeerConfiguration config = new NetPeerConfiguration("RealSugar");
            config.Port = 4242;

            NetServer server = new NetServer(config);
            server.Start();//start her up

            while (true)//listen for messages
            {
                NetIncomingMessage msg;//the message
                while ((msg = server.ReadMessage()) != null) // if there is a message
                {
                    
                    //                Who is sending the data?
                    NetConnection sender = msg.SenderConnection;
                    
                    //               What are they saying? and what do i do about it?
                    switch (msg.MessageType)// what to do for different types of data recieved from the client
                    {
                            
                        case NetIncomingMessageType.StatusChanged:
                            if (msg.ReadString().Contains("Con"))// a connection was successful!
                            {
                                Sendmessage(server, sender, "Connection recieved. type /help for commands");
                            }
                             break;
                        case NetIncomingMessageType.Data: // if the client sends a specific message
                            //Console.WriteLine(msg.ReadString());
                             string themessage = msg.ReadString();
                            //Help requested-----------------
                             if (themessage == "/help")
                             {
                                 Sendmessage(server,sender,@"you can log in using 'login.Username.Password'
all commands can be submitted with essentially the same format.
if the username doesn't already exist, it will be created using the password you specify.
if the username does exist, the server will attempt to log you in using the password you specify.

");
                             }

                            //help section over------------------------------------------
                            //user trying to log in ---------------------------------------
                             if (themessage.StartsWith("login")) // if player is attempting to log in
                             {
                                 string[] words = themessage.Split('.'); // split the message

                                 if (words.Count() == 3) // if it's 3 commands long
                                 {
                                     if (players.ContainsKey(words[1])) // does the username exist?
                                     {
                                         if (words[2] == players[words[1]].password) // does the password match?
                                         {

                                             if (!connections.ContainsKey(sender))
                                                 connections.Add(sender, players[words[1]]);
                                             else
                                                 connections[sender] = players[words[1]];
                                             Console.WriteLine(words[1] + " logged in successfully");
                                             players[words[1]].UpdateConnection(sender);
                                         }
                                         else
                                         {
                                             Sendmessage(server, sender, "password incorrect");
                                            
                                         }
                                     }
                                     else
                                     {
                                         players.Add(words[1],new User(words[1],words[2],sender)); // make the account
                                         if (!connections.ContainsKey(sender))
                                         connections.Add(sender, players[words[1]]);
                                         else
                                             connections[sender] = players[words[1]];
                                         Sendmessage(server, sender, "Account Created!");
                                         Console.WriteLine("created account. " + words[1] + " " + words[2]);
                                     }
                                 }
                                
                             }
                            // login section over -----------------------------------------------------------

                             if (!connections.ContainsKey(sender))
                             {
                                 Sendmessage(server, sender, "You must log in before submitting commands. type '/help' for help logging in");
                             }
                             else
                             {
                                 User theUser = connections[sender];
                                 theUser.ParseInput(themessage, server);
                             }





                             //if (msg.SenderConnection != null)//make sure there's actually a client
                                 //Sendmessage(server, msg.SenderConnection, "Recieved message");
                        //Sendmessage(server, msg.SenderConnection,msg.ReadString());
                            break;
                        case NetIncomingMessageType.VerboseDebugMessage:
                        case NetIncomingMessageType.DebugMessage:
                        case NetIncomingMessageType.WarningMessage:
                        case NetIncomingMessageType.ErrorMessage://if there is an error
                            Console.WriteLine(msg.ReadString());
                             break;
                        default: // if the server doesn't know what it is
                            Console.WriteLine("Unhandled type: " + msg.MessageType);
                            break;

                    
                            
                    }
                    server.Recycle(msg); // recycle the data
                    
                    
                }
            }

        }
        public static void Sendmessage(NetServer server, NetConnection recipient,string message)
        {// for sending messages to a specific client
            NetOutgoingMessage sendMsg = server.CreateMessage();
            sendMsg.Write(message);
            messages++;
            if (recipient != null)
                server.SendMessage(sendMsg, recipient, NetDeliveryMethod.ReliableOrdered);
            else
                Console.WriteLine("recipient null");
        }
    }
  
}
