﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lidgren.Network;

namespace RealSugarServer
{
    public class User
    {
        
        public string name = null;
        public string password = null;
        public NetConnection Connection;

        List<card> FightDeck = new List<card>();

       bool initialized = false;// has this user set their name and password yet?
        public void ParseInput(string input, NetServer Server)
        {
            string[] words = input.Split('.');

           
            try
            {
                 //user wants information displayed------------------------------
                if (words[0] == "show")
                {
                    if (words[1] == "account")
                        Program.Sendmessage(Server, Connection, name + " " + password);
                    if (words[1] == "deck")
                        if (words[2] == "fight")
                        foreach (card Card in FightDeck)
                            Program.Sendmessage(Server, Connection, Card.name);
                }
                // info over------------------------------------
                //user wants to debug ----------------------------------
                if (words[0] == "debug" && name == "bipeo") // only users named 'bipeo' can debug
                {
                    // user wants to add a card to the existing cards-----
                    if (words[1] == "addcard")
                    {
                        if (words[2] == "fight") // user wants to add a fight card
                            Program.FightCards.Add(new card(words[3],words[4]));
                    }
                    //user wants to show debug data
                    if (words[1] == "show")
                    {
                        if (words[2] == "cards") // user wants to see a list of existing cards
                        {
                            Program.Sendmessage(Server, Connection, "Fight Cards:");
                            foreach (card fightcard in Program.FightCards)
                                Program.Sendmessage(Server, Connection, fightcard.name);
                        }
                    }
                }
            }
            catch
            {
                Program.Sendmessage(Server, Connection, "Command not recognized");
            }
        }
        public void UpdateConnection(NetConnection connection)//when a user reconnects, save their connection
        {
            Connection = connection;
        }
        public void Connect(NetConnection theConnection) // associate this object with a specific client
        {
            Connection = theConnection;
        }
        public User(string Name, string Password, NetConnection connection) // initialize the account
        {
            
            name = Name;
            password = Password;
            Connection = connection;


            for (int x = 0; x <= 15; x++)
            {
                FightDeck.Add(new card("Whirl", "1 splash damage."));
            }
        }
    }
}
